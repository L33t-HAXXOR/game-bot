from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from datetime import datetime as dt
from datetime import timedelta as td

now = dt.now()
five_secs = now + td(seconds=5)
five_minutes = now + td(minutes=5)
thirty_secs = now + td(seconds=30)

print(now)
print(five_secs)

#Keep Chrome browser open after program finishes
chrome_options = webdriver.ChromeOptions()
chrome_options.add_experimental_option("detach", True)

driver = webdriver.Chrome(options=chrome_options)
driver.get("https://orteil.dashnet.org/experiments/cookie/")

cookie = driver.find_element(By.CSS_SELECTOR, value="#middle #cookie")

store = driver.find_element(By.CSS_SELECTOR, value="#store")





def buy_highest():
    money = int(driver.find_element(By.CSS_SELECTOR, value="#money").text.replace(',', ''))
    store = driver.find_element(By.ID, "store")
    store_elements = store.find_elements(By.TAG_NAME, 'div')
    store_elements.reverse()
    new_elements = store_elements[1::]
    # print(store_elements)
    for se in new_elements:
        # print("se:", se)
        try:
            store_item_text = se.find_element(By.CSS_SELECTOR, value="b").text
        except:
            continue
        # print('texty:', store_item_text)

        item_price = int(store_item_text.split(sep='- ')[1].replace(',', ''))
        if item_price < money:
            try:
                se.click()
                print("Purchased:", store_item_text)
            except:
                continue

        # splitter = se.text.split(sep='\n')[0].split(sep='- ')
        # # print('splitter here:', splitter)
        # if len(splitter) > 1:
        #     price = int(splitter[1].replace(',', ''))
        #     if money > price:
        #         try:
        #             print("Purchased:", se.text.split(sep='\n')[0])
        #             se.click()
        #         except UnboundLocalError:
        #             print('oofies! No data!')
        #             continue
        # print('se:', se.text)
        # print('oofies:', splitter)
        # print('\n')
        # if money > se:
        #     print('x:', se.text)


while dt.now() <= five_minutes:
    cookie.click()
    #if 5 seconds passed
    if dt.now() >= five_secs:
        #reset 5 second timer
        five_secs = dt.now() + td(seconds=5)
        #buy most expensive upgrade
        buy_highest()

        # for item in store:
        #     # get most expensive item and buy it
        #     print(item.text)
        #     pass



    # store_list = store.text.split(sep='\n')
    # buy_list = [i for i in store_list if store_list.index(i) % 2 == 0]
    # price_list = [int(i.split('- ')[1].replace(',', '')) for i in buy_list]
    # price_list.reverse()
    #
# driver.close()
print("quitting now")
driver.quit()
driver.quit()
